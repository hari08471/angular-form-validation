import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatFormFieldModule } from '@angular/material/form-field';
 import { MatInputModule } from '@angular/material/input'
 import { MatSelectModule } from '@angular/material/select'
 import { MatRadioModule } from '@angular/material/radio'
 import { MatButtonModule } from '@angular/material/button'
 import { MatSnackBarModule } from '@angular/material/snack-bar'
 
import { FormsModule } from '@angular/forms';
import { EmailvalidatorDirective } from './emailvalidator.directive';
import { PhoneNumberValidatorDirective } from './phone-number-validator.directive';
import { MyTemplateDrivenFormComponent } from './my-template-driven-form/my-template-driven-form.component';
import { PasswordPatternDirective } from './password-pattern.directive';
import { MatchPasswordDirective } from './match-password.directive';
import { ValidateUserNameDirective } from './validate-user-name.directive';

@NgModule({
  declarations: [
    AppComponent,
    EmailvalidatorDirective,
    PhoneNumberValidatorDirective,
    MyTemplateDrivenFormComponent,
    PasswordPatternDirective,
    MatchPasswordDirective,
    ValidateUserNameDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatButtonModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
