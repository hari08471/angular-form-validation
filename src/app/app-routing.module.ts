import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyTemplateDrivenFormComponent } from './my-template-driven-form/my-template-driven-form.component';

const routes: Routes = [
  {path: 'TemplateForm', component: MyTemplateDrivenFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
