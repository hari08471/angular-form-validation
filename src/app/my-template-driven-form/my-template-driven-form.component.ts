import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-my-template-driven-form',
  templateUrl: './my-template-driven-form.component.html',
  styleUrls: ['./my-template-driven-form.component.css']
})
export class MyTemplateDrivenFormComponent implements OnInit {

  ngOnInit(): void {
  }

  userModal = new User();  
  
  constructor() { }  
  
  onSubmit() {  
    alert('Form Submitted succesfully!!!\n Check the values in browser console.');  
    console.table(this.userModal);  
  }  

}
