import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

export interface Designation {
  id: string;
  value: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private _snackBar: MatSnackBar, private router: Router) { }

  ngOnInit(): void {
  }
  title = 'angular-form-validation';

  @ViewChild('myForm', { static: false })
  public MyForm!: NgForm;

  fname!:string;

  formData = {
    name:"",
    designation:"",
    gender:"",
    email: "",
    phone:""
  }

  

  designations: Designation[] = [
    {id: '1', value: 'Developer'},
    {id: '2', value: 'Senior Programmer'},
    {id: '3', value: 'Software Architect'}
  ];

  submitForm(e: any){
    console.log(this.MyForm);
    if(this.MyForm.valid){
        //Submit form logic here!
    }else{
      this._snackBar.open("Please fill all fields","",{duration:1000});
    }
  }

  TemplateDrivenForm(){
    this.router.navigate(['/TemplateForm']);
    
  }

}
